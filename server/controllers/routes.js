/**
 * Created by Warren.Watkinson on 4/15/2016.
 */
var express = require('express')
var router = express.Router();
var path = require('path');

var users = require(path.join(__dirname,'users'));

router
  .get('/partials/*', function(req,res) {
    console.log(path.join('..','..','client','app',req.params[0]));
    res.render(path.join('..','..','client','app',req.params[0]));
  })
  .get('/',function(req,res) {
    res.render('index');
  });

module.exports = router;
