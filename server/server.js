var express = require('express'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    path = require('path');

var routes = require(path.join(__dirname,'controllers','routes'));

var port = process.argv[2] || 8080;

var app = express();

// set views directory and engine to embedded JavaScript
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');

// add logging
app.use(logger('dev'));

// static route handling
app.use(express.static(path.join(__dirname,'..','client')));

// load controllers
app.use(routes);

/*
// parse url-encoded and json-encoded bodies
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
*/
app.listen(port, function() {
  var d = new Date();
  console.log(d.toUTCString() + ': Listening on port ' + port + '...');
});
