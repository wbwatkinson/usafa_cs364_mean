"use strict";
(function() {
  var app = angular.module('app', ['ngResource','ngRoute']);

  app.config(function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/partials/main/main',
        controller:'mainController'
      });
  });
})();
